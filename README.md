# Teoschool Project App : Backend

## Backend Docker image for the Spring Rest project App

---
Backend Maven driven project, which is mandatory for the front end with Angular.
You can find the front related files over here : https://gitlab.com/dart0/teoschool-project-app 
Service is exposed on port 9964 by default.

---
Building and running the image
---

Build with `docker build --rm -t your-image-name-here:tag DOCKERFILE_LOCATION`

Run with `docker run -d -p HOST_PORT:DEFAULT_PORT your-image-name-here:tag`